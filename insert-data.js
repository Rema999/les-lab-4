const readline = require('readline');
const fs = require('fs');
const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let data = {
    type: "water || qas",
    value: "00043",
    date: new Date('07-13-2021'),
    name: "Ramin"
};

function askSync(q) {
    return new Promise((resolve, reject) => {
        rl.question(q, (data) => {
            // TODO: Log the answer in a database
            console.log(data, 'received');
            if (false) {
                reject(new Error('some validation'))
            }
            ;

            resolve(data);
        });

    });
}

async function asker() {
    let name = await askSync("what is your name");
    let data = await askSync("what is data");
    let gas = await askSync("your update of gas ");


    rl.close();

    const path = './' + data + '.json';

    let result = {
        data, name, gas
    }

    fs.writeFileSync(path, JSON.stringify(result));

    let jsonData = require(path);
    console.log(jsonData);
    let jsonDataResult = Object.entries(jsonData).map(entry => ({[entry[0]]: entry[1]}));
    console.log(jsonDataResult);
    const csvWriter = createCsvWriter({
        path: 'out.csv',
        header: [
            {id: 'data', title: 'data'},
            {id: 'name', title: 'name'},
            {id: 'gas', title: 'gas'}
        ]
    });

    csvWriter .writeRecords(jsonDataResult)
        .then(() => console.log('The CSV file was written successfully'));
}

asker();



